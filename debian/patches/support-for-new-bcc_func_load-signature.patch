commit 0e5464f03ed2b72613e2e209e2fef8a4bbad5f30
Author: chantra <chantr4@gmail.com>
Date:   Tue Jul 5 09:04:34 2022 -0700

    [bcc] Support for new `bcc_func_load` signature.

    https://github.com/iovisor/bcc/commit/ffff0edc00ad249cffbf44d855b15020cc968536
    a new parameter was added to control the attach type.
    Subsequently, in
    https://github.com/iovisor/bcc/commit/815d1b84828c02ce10e1ea5163aede6b5786ba38
    , `-1` was used to signal the default behaviour.

    This change adds this new parameter to the call to `C.bcc_func_load`
    when LIBBCC_VERSION_CODE is greater or equal than 0.25.0, the next version to
    include the aforementioned change.

    Test plan:
    Against v0.25.0 is being tested as part of this project CI.

    also ran against the current v0.20.0 and ran CI: https://github.com/chantra/gobpf/runs/7436803048

---
 bcc/module.go |   25 ++++++++++++++++++++++++-
 1 file changed, 24 insertions(+), 1 deletion(-)

Index: b/bcc/module.go
===================================================================
--- a/bcc/module.go
+++ b/bcc/module.go
@@ -31,6 +31,29 @@ import (
 #cgo LDFLAGS: -lbcc
 #include <bcc/bcc_common.h>
 #include <bcc/libbpf.h>
+#include <bcc/bcc_version.h>
+
+#ifndef LIBBCC_VERSION_GEQ
+#define LIBBCC_VERSION_GEQ(a, b, c) 0
+#endif
+
+int bcc_func_load_wrapper(void *program, int prog_type, const char *name,
+						  const struct bpf_insn *insns, int prog_len,
+						  const char *license, unsigned kern_version,
+						  int log_level, char *log_buf, unsigned log_buf_size,
+						  const char *dev_name, int attach_type){
+
+#if LIBBCC_VERSION_GEQ(0, 25, 0)
+    return bcc_func_load(program, prog_type, name, insns, prog_len, license,
+						 kern_version, log_level, log_buf, log_buf_size,
+						 dev_name, attach_type);
+#else
+    return bcc_func_load(program, prog_type, name, insns, prog_len, license,
+						 kern_version, log_level, log_buf, log_buf_size,
+						 dev_name);
+#endif
+}
+
 */
 import "C"
 
@@ -227,7 +250,7 @@ func (bpf *Module) load(name string, pro
 		logBuf = make([]byte, logSize)
 		logBufP = (*C.char)(unsafe.Pointer(&logBuf[0]))
 	}
-	fd, err := C.bcc_func_load(bpf.p, C.int(uint32(progType)), nameCS, start, size, license, version, C.int(logLevel), logBufP, C.uint(len(logBuf)), nil)
+	fd, err := C.bcc_func_load_wrapper(bpf.p, C.int(uint32(progType)), nameCS, start, size, license, version, C.int(logLevel), logBufP, C.uint(len(logBuf)), nil, C.int(-1))
 	if fd < 0 {
 		return -1, fmt.Errorf("error loading BPF program: %v", err)
 	}
